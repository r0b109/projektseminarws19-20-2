from opcua import Server, ua, uamethod
import customMethods

PROTOCOL = "opc.tcp"
IPv4 = "0.0.0.0"
PORT = "4840"
SERVER_NAME = "OpcUa Test Server"
ENDPOINT = "/server/"


@uamethod
def schalter(parent):
    print("on")
    return True


# Singleton
class opcuaServer:
    __instance = None

    @staticmethod
    def getInstance():
        if opcuaServer.__instance is None:
            opcuaServer.__instance = Server()
        return opcuaServer.__instance

    @staticmethod
    def start(xmlFile):
        if opcuaServer.__instance is None:
            server = opcuaServer.getInstance()
            server.set_server_name(SERVER_NAME)
            server.set_endpoint(PROTOCOL + "://" + IPv4 + ":" + PORT + ENDPOINT)
            server.import_xml(xmlFile)

            uri = "http://org.htwdd.org/ProjSem"
            idx = server.register_namespace(uri)

            # add_methods in schleife und eine liste durchlaufen?
            # callMethod ->new Thread?
            # getVoltage durch generisches pull/push ersetzen
            #   ->Info aus Modell erforderlich (sowie params (als array uebergeben)

            # wann method im Model vs. def im code

            node = server.get_node('ns=2;i=5002')  # warum ns=2 und nicht 1 ?
            node.add_method(idx, 'schalter', customMethods.shift, [], [])

            # node.add_method(idx, 'shift', shift, [], [ua.VariantType.String])
            # liste/array mit node, name, type und vars...
            # customMethods.add(server, idx, )

            server.start()
        else:
            raise Exception("A OPC-UA Server is already running!")

    @staticmethod
    def stop():
        if opcuaServer.__instance is not None:
            server = opcuaServer.getInstance()
            server.stop()
